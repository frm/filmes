package br.com.ricarte.filmes.model.enuns;

public enum AccessConfig {

    BASE_FILM_API_URL("http://www.omdbapi.com/");

    private final String value;

    AccessConfig(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
