package br.com.ricarte.filmes.wsconsumer;

import android.util.Log;

import com.google.gson.GsonBuilder;

import br.com.ricarte.filmes.model.Search;
import br.com.ricarte.filmes.wsconsumer.resource.FilmResource;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static br.com.ricarte.filmes.model.enuns.AccessConfig.BASE_FILM_API_URL;

public class FilmApi {

    private static Retrofit retrofit;

    private static Retrofit getRetrofitSkipInstance() {
        return retrofit == null
                ? retrofit = new Retrofit.Builder()
                .baseUrl(BASE_FILM_API_URL.getValue())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient()
                        .setDateFormat("dd/MM/yyyy HH:mm:ss").create())).build()
                : retrofit;
    }

    public static Call<Search> getAllFilms() {
        Log.d("FilmApi", "FilmApi - getAllFilms() : ");
        return getRetrofitSkipInstance().create(FilmResource.class).getAllFilms();
    }

    public static Call<Search> getAllFilms(String s) {
        Log.d("FilmApi", "FilmApi - getAllFilms() : ");
        return getRetrofitSkipInstance().create(FilmResource.class).getAllFilms(s);
    }
}
