package br.com.ricarte.filmes.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.util.List;
import br.com.ricarte.filmes.R;
import br.com.ricarte.filmes.adapter.FilmsAdapter;
import br.com.ricarte.filmes.model.Search;
import br.com.ricarte.filmes.model.Search_;
import br.com.ricarte.filmes.wsconsumer.FilmApi;
import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static butterknife.ButterKnife.bind;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.edtSearch)
    EditText edtSearch;

    @BindView(R.id.listViewFilms)
    ListView listView;
    private Search search;
    private List<Search_> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Consultando Filme", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                consultFilms(edtSearch.getText().toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    private void consultFilms(String s) {
        Log.d("Film", "consultFilms: ");

        FilmApi.getAllFilms(s).enqueue(new Callback<Search>() {
            @Override
            public void onResponse(@NonNull Call<Search> request, @NonNull Response<Search> response) {
                Integer statusCode = response.code();

                switch (statusCode) {
                    case 200:
                        Log.d("MainActivity", "list: " + statusCode);
                        search = response.body();
                        list = search.getSearch();
                        if (list == null) {
                            Toast.makeText(getApplicationContext(), "LISTA VAZIA", Toast.LENGTH_SHORT).show();
                        } else {
                            configureScreen();
                        }
                        break;
                    case 401:
                        Log.e(MainActivity.class.getSimpleName(), "Filmes: Não autorizado!");
                        break;
                    default:
                        Log.e(MainActivity.class.getSimpleName(), "Filmes: Erro Desconhecido! " + response.toString());
                        break;
                }
            }

            @Override
            public void onFailure(@NonNull Call<Search> call, @NonNull Throwable t) {
                Log.e(MainActivity.class.getSimpleName(), t.toString());
            }
        });

    }

    private void configureScreen() {

        FilmsAdapter adapter = new FilmsAdapter(list, this);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((adapterView, view, position, id) ->
                filmDetails(adapterView.getItemAtPosition(position)));


    }

    private void filmDetails(Object itemAtPosition) {
        Log.d("detail", "Obj: " +itemAtPosition);

        Search_ item = (Search_) itemAtPosition;


        Intent itFilmDetail = new Intent(getApplicationContext(), FilmDetailsActivity.class);
        itFilmDetail.putExtra("title", item.getTitle());
        itFilmDetail.putExtra("year", item.getYear());
        itFilmDetail.putExtra("imdbID", item.getImdbID());
        itFilmDetail.putExtra("type", item.getType());
        itFilmDetail.putExtra("poster", item.getPoster());

        startActivity(itFilmDetail);

    }
}
