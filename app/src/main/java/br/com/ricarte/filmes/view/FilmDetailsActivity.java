package br.com.ricarte.filmes.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import br.com.ricarte.filmes.R;
import butterknife.BindView;

import static butterknife.ButterKnife.bind;

public class FilmDetailsActivity extends AppCompatActivity {

    @BindView(R.id.title)
    TextView titleT;

    @BindView(R.id.year)
    TextView yearT;

    @BindView(R.id.imdbID)
    TextView imdbIDT;

    @BindView(R.id.type)
    TextView typeT;

    @BindView(R.id.poster)
    TextView posterT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_details);
        bind(this);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Intent itFilmDetail = getIntent();
        if (itFilmDetail != null) {
            String title = itFilmDetail.getStringExtra("title");
            String year = itFilmDetail.getStringExtra("year");
            String imdbID = itFilmDetail.getStringExtra("imdbID");
            String type = itFilmDetail.getStringExtra("type");
            String poster = itFilmDetail.getStringExtra("poster");

            configureScreen(title, year, imdbID, type, poster);
        }
    }

    private void configureScreen(String title, String year, String imdbID, String type, String poster) {

        titleT.setText(title);
        yearT.setText(year);
        imdbIDT.setText(imdbID);
        typeT.setText(type);
        posterT.setText(poster);

    }

}
