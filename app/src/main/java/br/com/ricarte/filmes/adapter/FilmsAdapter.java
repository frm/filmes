package br.com.ricarte.filmes.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.ricarte.filmes.R;
import br.com.ricarte.filmes.model.Search_;
import br.com.ricarte.filmes.view.MainActivity;

public class FilmsAdapter extends BaseAdapter {

    private MainActivity activity;
    private List<Search_> searchList;

    public FilmsAdapter(List<Search_> searchList, MainActivity activity) {
        this.activity = activity;
        this.searchList = searchList;
    }

    @Override
    public int getCount() {
        return searchList.size();
    }

    @Override
    public Object getItem(int position) {
        return searchList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.item_films_layout,
                    parent, false);
        }

        Search_ search_ = searchList.get(position);

        TextView title = convertView.findViewById(R.id.txvTitle);
        TextView year = convertView.findViewById(R.id.txvYear);
        TextView type = convertView.findViewById(R.id.txvType);

        title.setText("TITULO: " + search_.getTitle());
        year.setText("Ano: " + search_.getYear());
        type.setText("Tipo: " + search_.getType());

        return convertView;
    }
}
