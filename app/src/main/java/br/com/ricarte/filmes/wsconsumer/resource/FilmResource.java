package br.com.ricarte.filmes.wsconsumer.resource;

import br.com.ricarte.filmes.model.Search;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FilmResource {

    @GET("?i=tt3896198&apikey=bd52e9d4&s=Batman&page=2")
    Call<Search> getAllFilms();

    @GET("?i=tt3896198&apikey=bd52e9d4")
    Call<Search> getAllFilms(
            @Query("s") String s);
}
